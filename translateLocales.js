const DE = require("dotenv");
DE.config({ path: "./env" });

const TJO = require('translate-json-object')();
TJO.init({
    googleApiKey: process.env.GOOGLE_TRANSLATE_API_KEY
});

const fs = require("mz/fs");
const languages = ["ar", "eu", "en", "es", "de", "it", "hi", "ja", "ko", "ru", "tr", "th"]

const main = async function () {
    const translations = [];
    const fr = JSON.parse(fs.readFileSync(`./locales/fr.json`));

    fs.readdirSync("./locales").forEach((item) => {
        if (item === "fr.json" || item === "index.js")
            return
        fs.unlinkSync(`./locales/${item}`);
    });

    languages.forEach((language, index) => {
        translations.push(new Promise((resolve) => {
            setTimeout(async () => {
                const translated = await TJO.translate(fr, language);
                translated["set.code"] = translated["set.code"].replace("`` `JS {0}` ``", "```JS\n{0}```")
                fs.writeFileSync(`./locales/${language}.json`, JSON.stringify(translated, null, 2))
                console.log(language);
                resolve();
            }, index * 500);
        }));
    });

    await Promise.all(translations);
}

main();