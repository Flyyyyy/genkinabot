import "babel-polyfill";
import * as database from "./"

async function main() {
    let user = await database.getUser("170994259690782720")
    console.log("USER");
    console.log(await database.save(user));

    let channel = await database.getChannel({ id: "219215920369893376", position: "ddd", guild: { id: "194530908798320641" } });
    console.log("CHANNEL");
    console.log(await database.save(channel));

    let guild = await database.getGuild({ id: "194530908798320641", ownerId: "138271155164413952", defaultChannel: "ddddd" });
    console.log("GUILD");
    console.log(await database.save(guild));

    database.forceSave();
    console.log(database.getCache());
}

main();