import { default as datastore } from "@google-cloud/datastore";
import { Datastore as config } from "../../configs/"
import { Channel, Guild, Log, User } from "./models";

const Datastore = datastore({
    projectId:   config.project,
    keyFilename: config.keyFile
});

const Keys = Object.freeze({
    user:    (id) => Datastore.key({ namespace: config.namespace, path: ["User", id] }),
    channel: (id) => Datastore.key({ namespace: config.namespace, path: ["Channel", id] }),
    guild:   (id) => Datastore.key({ namespace: config.namespace, path: ["Guild", id] }),
    log:     (id) => Datastore.key({ namespace: config.namespace, path: ["Log", id] })
});

const Types = Object.freeze({
    ChannelDB: 0,
    UserDB:    1,
    GuildDB:   2,
    MessageDB: 3,

    ChannelEris: 3,
    UserEris:    4,
    GuildEris:   5,
    MessageEris: 6,

    ChannelID: 7,
    UserID:    8,
    GuildID:   9,
    MessageID: 10
});

const Cache = {};
let CacheChanges = 0;


/**
 * Get the database item of a Guild
 * @param {(ErisMessage)} guild The ErisGuild or something that can relate to it
 * @returns {DBItems} The database items
 * @returns {DBItems.guild} The guild item
 * @returns {DBItems.channel} The channel item
 * @returns {DBItems.user} The user item
 */
const _getItems = async (erisMessage) => {
    const keys = [];
    const items = {};
    const ids = {
        user:    getId(erisMessage, Types.UserID),
        channel: getId(erisMessage, Types.ChannelID),
        guild:   getId(erisMessage, Types.GuildID)
    };

    if (typeof Cache[`User:${ids.user}`] !== "undefined")
        items.user = Cache[`User:${ids.user}`];
    else
        keys.push(Keys.user(ids.user));

    if (typeof Cache[`Channel:${ids.channel}`] !== "undefined")
        items.channel = Cache[`Channel:${ids.channel}`];
    else
        keys.push(Keys.channel(ids.channel));

    if (typeof Cache[`Guild:${ids.guild}`] !== "undefined")
        items.guild = Cache[`Guild:${ids.guild}`];
    else
        keys.push(Keys.guild(ids.guild));

    if (keys.length === 0)
        return items;

    const results = await Datastore.get(keys);

    results[0].forEach(entity => {
        const symb = Object.getOwnPropertySymbols(entity)[0];
        switch (entity[symb].kind) {
            case "User":
                items.user = entity;
                break;
            case "Channel":
                items.channel = entity;
                break;
            case "Guild":
                items.guild = entity;
                break;
        }
    });

    if (typeof items.user === "undefined")
        items.user = User({ id: ids.user });
    if (typeof items.channel === "undefined")
        items.channel = Channel({ id: ids.channel, guildID: erisMessage.channel.guild.id });
    if (typeof items.guild === "undefined")
        items.guild = Guild({ id: ids.guild });
    return items;
}


/**
 * Get the database item of a User
 * @param {(ErisMessage|ErisUser|UserID)} user The ErisUser or something that can relate to it
 * @returns {DBUser} The database item
 */
const _getUser = async (eris) => {
    const id = getId(eris, Types.UserID);

    if (typeof Cache[`User:${id}`] !== "undefined")
        return Cache[`User:${id}`];

    try {
        const db = await Datastore.get(Keys.user(id));

        if (typeof db[0] !== "undefined")
            return db[0];
        else {
            return User({ id: id })
        }
    }
    catch (error) {
        log({
            type:  "ERROR",
            cause: "Unknown error while trying to get a user",
            error: JSON.stringify(error)
        });
    }
};


/**
 * Get the database item of a Channel
 * @param {(ErisChannel|ErisMessage|ErisChannel|ChannelID)} channel The ErisChannel or something that can relate to it
 * @returns {DBChannel} The database item
 */
const _getChannel = async (eris) => {
    const id = getId(eris, Types.ChannelID);

    if (typeof Cache[`Channel:${id}`] !== "undefined")
        return Cache[`Channel:${id}`];

    try {
        let db = await Datastore.get(Keys.channel(id));
        if (typeof db[0] !== "undefined")
            return db[0];
        else {
            return Channel({ id: id })
        }
    }
    catch (error) {
        log({
            type:  "ERROR",
            cause: "Unknown error while trying to get a channel",
            error: JSON.stringify(error)
        });
    }
};


/**
 * Get the database item of a Guild
 * @param {(ErisChannel|ErisMessage|ErisGuild|GuildID)} guild The ErisGuild or something that can relate to it
 * @returns {DBGuild} The database item
 */
const _getGuild = async (eris) => {
    let id = getId(eris, Types.GuildID);

    if (typeof Cache[`Guild:${id}`] !== "undefined")
        return Cache[`Guild:${id}`];

    try {
        let db = await Datastore.get(Keys.guild(id));
        if (typeof db[0] !== "undefined")
            return db[0];
        else {
            return Guild({ id: id })
        }
    }
    catch (error) {
        log({
            type:  "ERROR",
            cause: "Unknown error while trying to get a guild",
            error: JSON.stringify(error)
        });
    }
};


const getId = (eris, requested) => {
    let type;

    if (typeof eris === "string")
        return eris;

    if (eris.position !== undefined) type = Types.ChannelEris;
    else if (eris.bot !== undefined) type = Types.UserEris
    else if (eris.defaultChannel !== undefined) type = Types.GuildEris;
    else if (eris.author !== undefined) type = Types.MessageEris
    else return;

    switch (requested) {
        case Types.ChannelID:
            switch (type) {
                case Types.ChannelEris: return eris.id;
                case Types.MessageEris: return eris.channel.id;
                default: throw "Invalid arguments";
            }

        case Types.UserID:
            switch (type) {
                case Types.UserEris: return eris.id;
                case Types.MessageEris: return eris.author.id;
                default: throw "Invalid arguments";
            }

        case Types.GuildID:
            switch (type) {
                case Types.GuildEris: return eris.id;
                case Types.ChannelEris: return eris.guild.id;
                case Types.MessageEris: return eris.channel.guild.id;
                default: throw "Invalid arguments";
            }

        case Types.MessageID:
            switch (type) {
                case Types.MessageEris: return eris.id;
                default: throw "Invalid arguments";
            }
    }
}


/**
 * Log something on the database and console
 * @param {Object} toLog Object representing a log in database
 * @param {string} toLog.type Type of the log (ERROR, INFO, WARNING) are both valids inputs
 * @param {Object} toLog.error If the log is an error, this should be an object with details about the error
 * @param {string} toLog.cause Why this log is existing ?
 * @param {Object} toLog.discord If the error is related to discord, give more informations about how it happened
 * @param {ErisMessage} toLog.discord.message the 
 * @param {DBUser} toLog.discord.user The DB entry of the user that caused error
 * @param {DBChannel} toLog.discord.channel The channel where the error occured, it should contains channel DB entry
 * @param {DBGuild} toLog.discord.guild The guild where the error occured, it should contains guild DB entry
 * @returns {DBLog} The logged entry
 */
const _log = async (toLog) => {
    console.log(toLog);
    //idk what this is doing, but it seems cool
    if (toLog.discord)
        if (toLog.discord.message !== undefined) {
            if (toLog.discord.message.author)
                toLog.discord.message = {
                    content: toLog.discord.message.content,
                    author:  {
                        username: toLog.discord.message.author.username,
                        id:       toLog.discord.message.author.id
                    }
                };
            else
                toLog.discord.message = {
                    content: toLog.discord.message.content
                };


            if (toLog.discord.guild !== undefined)
                toLog.discord.guild = {
                    name: toLog.discord.guild.name,
                    id:   toLog.discord.guild.id
                };

            if (toLog.discord.user !== undefined)
                toLog.discord.user = {
                    username: toLog.discord.user.username,
                    id:       toLog.discord.user.id
                }
        }

    await Datastore.save({
        key:                Keys.log(Math.floor(Math.random() * 999999999)),
        data:               Log(toLog),
        excludeFromIndexes: ["error.stack", "error", "cause", "discord"]
    });
    return Log(toLog);
}


const _addToSave = async (entity) => {
    let symb = Object.getOwnPropertySymbols(entity)[0];

    if (typeof symb !== "symbol") {
        let keyGetter;
        if (typeof entity.globalAdmin !== "undefined") keyGetter = Keys.user;
        else if (typeof entity.guildID !== "undefined") keyGetter = Keys.channel;
        else keyGetter = Keys.guild;

        entity[Symbol("KEY")] = keyGetter(entity.id)
        symb = Object.getOwnPropertySymbols(entity)[0];
    }

    Cache[`${entity[symb].kind}:${entity[symb].name}`] = entity;
    CacheChanges++;

    if (CacheChanges >= config.cacheSize) return _forceSave();
    else
        return;
}

const _forceSave = async () => {
    let entities = [];

    Object.keys(Cache).forEach(async key => {
        let entityKey;
        if (key.startsWith("User")) entityKey = Keys.user(key.replace("User:", ""));
        if (key.startsWith("Channel")) entityKey = Keys.channel(key.replace("Channel:", ""));
        if (key.startsWith("Guild")) entityKey = Keys.guild(key.replace("Guild:", ""));

        entities.push({
            key:                entityKey,
            data:               Cache[key],
            excludeFromIndexes: ["messagesStats"]
        });

        delete Cache[key];
    });

    CacheChanges = 0;

    await Datastore.save(entities);
    return;
};


export const getItems = _getItems;
export const getUser = _getUser;
export const getChannel = _getChannel;
export const getGuild = _getGuild;
export const log = _log;
export const save = _addToSave;
export const forceSave = _forceSave;
export const getCache = () => { return Cache };