export default (object) => {
    return Object.assign({}, {
        id:      "?",
        guildID: "?",

        date: new Date(),

        messagesStats: {
            messages:       0,
            messagesByUser: {},
            images:         0,
            imagesByUser:   {},
            averageLength:  0
        }
    }, object);
}