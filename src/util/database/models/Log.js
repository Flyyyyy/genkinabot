export default (object) => {
    return Object.assign({}, {
        date: new Date()
    }, object);
}