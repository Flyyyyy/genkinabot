export default (object) => {
    return Object.assign({}, {
        id:          "?",
        globalAdmin: false,
        date:        new Date(),

        musicBot: {},

        messagesStats: {
            messages:          0,
            messagesByGuild:   {},
            messagesByChannel: {},
            images:            0,
            imagesByGuild:     {},
            imagesByChannel:   {},
            averageLength:     0
        }
    }, object);
}