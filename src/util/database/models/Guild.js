export default (object) => {
  return Object.assign({}, {
    id:   "?",
    date: new Date(),

    settings: {
      prefix:         "!",
      locale:         "fr",
      deleteCommands: true
    },

    modules: {
      enabled: (process.env.GENKINA_MODULES || "").split(" ").length < 1 ? ["commands", "messagesStats", "baseCommands"] : (process.env.GENKINA_MODULES || "").split(" ")
    },

    commands: {
      enabled: []
    },

    code: {
      actions:  2,
      isBugged: true
    },

    bet: {},

    moderation: {},

    musicBot: {
      repeat:  false,
      queue:   [],
      results: 3
    },

    messagesStats: {
      messages:          0,
      messagesByUser:    {},
      messagesByChannel: {},
      images:            0,
      imagesByUser:      {},
      imagesByChannel:   {},
      averageLength:     0
    }
  }, object);
}