import * as Locales from "./locales";

export class Messages {
    constructor(bot) {
        this.locale = Locales[bot.guild.settings.locale] || Locales.en;

        this.colors = {
            green:  "11403148",
            red:    "16613247",
            orange: "16761721",
            blue:   "7455229",
            yellow: "16252792"
        };

        this.get = (message, ...args) => {
            let str = this.locale[message];
            if (this.locale[message] === undefined)
                str = Locales.en_UK[message];

            if (args.length > 0)
                return str.replace(/{(\d+)}/g, (match, number) => {
                    if (args[number] !== undefined)
                        return args[number]
                    else
                        return match;
                });
            else
                return str;
        };

        this.eval = (status, toEval, evaled, sw) => {
            return this.basicEmbed({
                color:  status ? this.colors.green : this.colors.red,
                fields: [{
                    name:   "Value to evaluate",
                    value:  "```JS\n" + toEval + "```",
                    inline: false
                }, typeof evaled === "object" ? {
                    name:   "Value evaled to",
                    value:  "```JSON\n" + JSON.stringify(evaled, null, 2) + "```",
                    inline: false
                } : {
                        name:   "Value evaled to",
                        value:  "```JS\n" + evaled + "```",
                        inline: false
                    }],
                footer: {
                    text: `Evaled in ${sw}ms`
                }
            });
        }

        this.permissions = (permissionRequired) => {
            return this.basicEmbed({
                color:  this.colors.orange,
                fields: [{
                    name:   this.get("error.title"),
                    value:  this.get("error.permission", permissionRequired),
                    inline: false
                }]
            });
        };

        this.error = (err) => {
            return this.basicEmbed({
                color:  this.colors.red,
                fields: [{
                    name:   this.get("error.title"),
                    value:  err,
                    inline: false
                }]
            });
        };

        this.valid = (msg) => {
            return this.basicEmbed({
                color:  this.colors.green,
                fields: [{
                    name:   this.get("valid.title"),
                    value:  msg,
                    inlina: false
                }]
            });
        }

        this.basicEmbed = (filler) => {
            return {
                content: "\n",
                embed:   Object.assign({}, {
                    color:  "11403148",
                    fields: [
                        {
                            name:   `example field name`,
                            value:  "field value",
                            inline: false
                        }
                    ],
                    footer: {
                        text:     this.get("ad.getgenkina"),
                        icon_url: "http://i.imgur.com/fakUZh6.png"
                    }
                }, filler)
            }
        };
    }
}