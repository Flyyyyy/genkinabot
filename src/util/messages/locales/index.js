import fs from "mz/fs";

fs.readdirSync("./locales").forEach((item) => {
    if (item === "index.js")
        return;

    exports[item.replace(".json", "")] = JSON.parse(fs.readFileSync(`./locales/${item}`, "utf8"));
});