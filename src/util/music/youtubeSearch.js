import * as moment from "moment";
import * as rp from "request-promise";

export default async (bot, query, maxRes) => {
    let response = {};
    let videos = [];
    let playlists = [];
    let queries = [];

    try {
        let results = rp({ //Youtube querry
            uri: "https://www.googleapis.com/youtube/v3/search",
            qs:  {
                part:       "snippet", //we dont have anything else than snippet available on youtube/search
                maxResults: maxRes, //max results are given as argument
                q:          query, //query is the research of the user
                safeSearch: "moderate", //lets try to dont have nsfw things as music (:
                type:       "video,playlist", //we dont want to have channels as return, only videos and TODO playlists SOON TM
                key:        bot.config.youtube.apiKey //this is the api key given on the config.js file
            },
            json:  true, //we want to have the query parsed as json
            proxy: bot.config.youtube.proxy ? `http://${bot.config.youtube.proxy}:${bot.config.youtube.proxyPort}` : false //The proxy
        });

        results.items.forEach((item) => { //for each videos / playlist returned
            response[item.id.videoId || item.id.playlistId] = { //we add it to the response object
                title:  item.snippet.title, //video title
                author: item.snippet.channelTitle, //the name of the youtube channel that added it
                source: item.id.kind, //youtube#video or youtube#playlist
                id:     item.id.videoId || item.id.playlistId
            };
        });

        Object.keys(response).forEach((key) => {
            if (response[key].source === "youtube#video")
                videos.push(response[key].id);
            else
                playlists.push(response[key].id);
        });

        queries.push(new Promise(async (reso, reje) => {
            try {
                let results = await rp({ //if it is, query to get more details about it 
                    uri: "https://www.googleapis.com/youtube/v3/videos", //video informations endpoint
                    qs:  {
                        part: "contentDetails,statistics", //contentDetails for durations, and statistics for like rate
                        id:   videos.join(","), //the id of the video
                        key:  bot.config.youtube.apiKey //this is the api key given on the config.js file
                    },
                    json:  true, //we want to have the query parsed as json
                    proxy: bot.config.youtube.apiKey ? `http://${bot.config.youtube.proxy}:${bot.config.youtube.proxyPort}` : false //The proxy
                });

                results.items.forEach((item) => {
                    response[item.id].duration = moment.duration(item.contentDetails.duration).format("h:mm:ss");
                    response[item.id].views = item.statistics.viewCount; //views of the video
                    response[item.id].likes = item.statistics.likeCount; //likes of the video
                    response[item.id].dislikes = item.statistics.dislikeCount; //dilikes of the video
                });

                reso();
            }
            catch (error) {
                bot.log({ //we log it on database
                    type:  "ERROR",
                    cause: "Trying to query youtube video api",
                    error: error.toString()
                });
                reje(error);
            }
        }));

        queries.push(new Promise(async (reso, reje) => {
            try {
                let results = await rp({ //if it is, query to get more details about it 
                    uri: "https://www.googleapis.com/youtube/v3/playlists", //video informations endpoint
                    qs:  {
                        part: "contentDetails", //contentDetails for durations, and statistics for like rate
                        id:   playlists.join(","), //the id of the video
                        key:  bot.config.youtube.apiKey //this is the api key given on the config.js file
                    },
                    json:  true, //we want to have the query parsed as json
                    proxy: bot.config.youtube.proxy ? `http://${bot.config.youtube.proxy}:${bot.config.youtube.proxy}` : false //The proxy
                });

                results.items.forEach(function (item) {
                    response[item.id].items = item.contentDetails.itemCount;
                });

                reso();
            }
            catch (error) {
                bot.log({ //we log it on database
                    type:  "ERROR",
                    cause: "Trying to query youtube playlist api",
                    error: error.toString()
                });
                reje(error);
            }
        }));

        await Promise.all(queries);

        let retu = [];
        Object.keys(response).forEach(key => retu.push(response[key]));
        return retu;
    }
    catch (error) {
        bot.log({
            type:  "ERROR",
            cause: "Trying to query youtube search api",
            error: error.toString()
        });
    }
};