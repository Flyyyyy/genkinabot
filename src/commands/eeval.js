import * as sw from "node-stopwatch";

export const meta = {
  name:         "eeval", //Name of the module
  partOfModule: "baseCommands", //The MAIN module of that commands, so if this module is enabled, the command is too
  help:         "Eval a JavaScript code", //A small descriptive that explain what the command do
  permission:   "globalAdmin",
  author:       "Fly" //who wrote that command ?
};

export const run = async (bot, m, args) => {
  let watch = sw.Stopwatch.create(); //create a stopwatch from "node-stopwatch"
  let toEval = args.replace("```JS", "").replace("```", "").replace("```", ""); //the text to evalutate
  try {//a try and catch, so the evaled code won't create any shit on the main process
    let beforeEval = JSON.stringify({
      guild: bot.guild,
      user:  bot.user
    }); //create a snapshot of DB entry before evaling code 

    watch.start(); //start the timer, to check how long the V8 take to eval the text
    let evaled = eval(toEval);

    
    if(typeof evaled === "object" && typeof evaled.then === "function")
      evaled = await evaled();
    watch.stop(); //stop the timer
    
    m.channel.createMessage(bot.messages.eval(true, toEval, evaled, watch.elapsedMilliseconds)); //send a message with how the v8 evaled the code
    bot.log({
      type:    "INFO",
      cause:   "Someone used eeval without error",
      discord: {
        message: m
      }
    });
    let afterEval = JSON.stringify({
      guild: bot.guild,
      user:  bot.user
    });

    if (afterEval == beforeEval)
      return "OK";
    else
      return {
        guild: bot.guild,
        user:  bot.user
      }
  }

  catch (ex) { //if the eval throw an error
    watch.stop(); //stop the timer
    m.channel.createMessage(bot.messages.eval(false, toEval, ex.toString(), watch.elapsedMilliseconds)); //send a message with how the v8 evaled the code 
    bot.log({
      type:    "INFO",
      cause:   "Someone used eeval with an error",
      discord: {
        message: m
      }
    });
    return "OK";
  }
};
