import * as Locales from "../util/messages/locales/"

export const meta = {
    name:         "set", //Name of the module
    partOfModule: "baseCommands", //The MAIN module of that commands, so if this module is enabled, the command is too
    help:         "Manage settings of the server", //A small descriptive that explain what the command do
    permission:   "manageGuild",
    author:       "Fly" //who wrote that command ?
};

export const run = async (bot, m, args) => {
    let key = args.split(" ")[0] || null;
    let value = args.split(" ")[1] || null;

    if (key === null || value === null) {
        bot.private(m.author, bot.messages.error(bot.messages.get("set.usage")));
        return "OK";
    }

    switch (key) {
        case "deleteCommands": {
            if (value === "true")
                bot.guild.settings.deleteCommands = true;
            else if (value === "false")
                bot.guild.settings.deleteCommands = false;
            else {
                m.channel.createMessage(bot.messages.error(bot.messages.get("set.badvalue", value)));
                return "OK";
            }

            m.channel.createMessage(bot.messages.valid(bot.messages.get("set.done", key, value)));
            return { guild: bot.guild };
        }

        case "prefix": {
            if (value.length > 4) {
                m.channel.createMessage(bot.messages.error(bot.messages.get("set.badvalue", value)));
                return "OK";
            }

            bot.guild.settings.prefix = value;
            m.channel.createMessage(bot.messages.valid(bot.messages.get("set.done", key, value)));
            return { guild: bot.guild };
        }

        case "locale": {
            if (Locales[value] === undefined) {
                m.channel.createMessage(bot.messages.error(bot.messages.get("set.badvalue", value)));
                return "OK";
            }

            bot.guild.settings.locale = value;
            bot.messages.locale = Locales[value];
            m.channel.createMessage(bot.messages.valid(bot.messages.get("set.done", key, value)));
            return { guild: bot.guild };
        }

        case "code": {
            let code = args.replace("code ", "").replace("```JS", "").replace("```", "").replace("```", "");
            if (code.includes("for") || code.includes("while")) {
                m.channel.createMessage(bot.messages.error(bot.messages.get("set.badvalue", code)));
                return "OK";
            }

            bot.guild.code.source = code;
            bot.guild.code.isBugged = false;
            bot.guild.code.errorMessage = null;

            m.channel.createMessage(bot.messages.valid(bot.messages.get("set.code", code)));

            return { guild: bot.guild };
        }

        case "music": {
            if (value.toLowerCase() === "text") {
                bot.guild.musicBot.text = m.channel.id;
                m.channel.createMessage(bot.messages.valid(bot.messages.get("set.done", "music.text", m.channel.name)));
                return { guild: bot.guild };
            }

            else if (value.toLowerCase() === "voice") {
                if (m.member.voiceState === undefined || typeof m.member.voiceState.channelID !== "string") {
                    m.channel.createMessage(bot.messages.error(bot.messages.get("music.novoice")));
                    return "OK";
                }
                bot.guild.musicBot.voice = m.member.voiceState.channelID;
                m.channel.createMessage(bot.messages.valid(bot.messages.get("set.done", "music.voice", m.channel.guild.channels.find(chan => { return chan.id === bot.guild.musicBot.voice; }).name)));
                return { guild: bot.guild };
            }

            else {
                m.channel.createMessage(bot.messages.error(bot.messages.get("set.badvalue", value)));
                return "OK";
            }
        }

        default:
            m.channel.createMessage(bot.messages.error(bot.messages.get("set.badkey")));
            return "OK";
    }
};