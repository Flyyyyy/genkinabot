import * as fs from "mz/fs";
import * as Commands from "../commands/"
import * as rp from "request-promise";
import { transform } from 'babel-core';

export const meta = {
  name:   "commands",
  desc:   "Core module, used to use the commands",
  author: "Fly"
};

export const messageCreate = main;
export const messageUpdate = main;

async function main(bot, message) {
  let command;
  let args;

  if (message.content.startsWith(bot.guild.settings.prefix)) { //if the message aint starting with prefix, skip
    command = message.content.split(" ")[0].replace(bot.guild.settings.prefix, "").toLowerCase() //get the command string
    args = message.content.split(" "); args.shift(); args = args.join(" "); //arguments for using commands with g!
  }

  else if (message.content.startsWith("g!")) { //if the message aint starting with prefix, skip
    command = message.content.split(" ")[0].replace("g!", "").toLowerCase() //get the command string
    args = message.content.split(" "); args.shift(); args = args.join(" "); //arguments for using commands with g!
  }

  else
    return "OK";

  if (Commands[command] === undefined) //the command don't exist !
    return "OK"; //skip

  if (!bot.guild.commands.enabled.includes(Commands[command].meta.name)) { //if the command IS NOT ENABLED
    if (Commands[command].meta.partOfModule === undefined) //if the commmand DONT have a "partOfModule"
      return "OK"; //skip
    if (!bot.guild.modules.enabled.includes(Commands[command].meta.partOfModule)) //if the "partOfModule" module is NOT enabled 
      return "OK"; //skip
  }

  if (message.member.permission.has(Commands[command].meta.permission) || bot.user.globalAdmin) {
    try {
      bot.request = rp;
      bot.files = fs;
      bot.babel = async (code) => {
        let config = JSON.parse(await fs.readFile(__dirname.replace("lib/modules", ".babelrc"), "utf8"));
        return transform(code, config).code;
      };

      let result = await Commands[command].run(bot, message, args); //execute the command

      if (bot.guild.settings.deleteCommands) //if this is set to true, delete the message
        message.delete();

      if (typeof result === "object") {
        return {
          guild:   result.guild || bot.guild,
          user:    result.user || bot.user,
          channel: result.channel || bot.channel
        }
      }
      else if (result === "OK")
        return "OK";
      else {
        bot.log({ //log it to database
          type:  "ERROR",
          cause: `Trying to use ${Commands[command]} !`,
          error: { message: "No return !" }
        });
        return "OK";
      }
    }

    catch (error) { //if the command throw an error
      bot.log({ //log it
        type:  "ERROR",
        cause: "Trying to execute command",
        error: { command: command, error: error.toString() || undefined }
      });
    }
  }

  else {
    bot.private(message.author, bot.messages.permissions(Commands[command].meta.permission));
    return "OK";
  }
}