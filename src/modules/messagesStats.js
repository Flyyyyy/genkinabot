
export const meta = {
  name:        "messagesStats",
  description: "Counting messages to the database",
  author:      "Fly"
};

export const messageCreate = (bot, m) => {
  //GUILD
  bot.guild.messagesStats.messages++;

  if (bot.guild.messagesStats.messagesByUser[m.author.id] !== undefined)
    bot.guild.messagesStats.messagesByUser[m.author.id]++;
  else bot.guild.messagesStats.messagesByUser[m.author.id] = 1;

  if (bot.guild.messagesStats.messagesByChannel[m.channel.id] !== undefined)
    bot.guild.messagesStats.messagesByChannel[m.channel.id]++;
  else bot.guild.messagesStats.messagesByChannel[m.channel.id] = 1;
  bot.guild.messagesStats.averageLength = calculateAverageMessages(bot.guild.messagesStats.averageLength, bot.guild.messagesStats.messages, m.content.length);



  //USER
  bot.user.messagesStats.messages++;

  if (bot.user.messagesStats.messagesByGuild[m.channel.guild.id] !== undefined)
    bot.user.messagesStats.messagesByGuild[m.channel.guild.id]++;
  else bot.user.messagesStats.messagesByGuild[m.channel.guild.id] = 1;

  if (bot.user.messagesStats.messagesByChannel[m.channel.id] !== undefined)
    bot.user.messagesStats.messagesByChannel[m.channel.id]++;
  else bot.user.messagesStats.messagesByChannel[m.channel.id] = 1;
  bot.user.messagesStats.averageLength = calculateAverageMessages(bot.user.messagesStats.averageLength, bot.user.messagesStats.messages, m.content.length);



  //CHANNEL
  bot.channel.messagesStats.messages++;
  if (bot.channel.messagesStats.messagesByUser[m.author.id] !== undefined)
    bot.channel.messagesStats.messagesByUser[m.author.id]++;
  else bot.channel.messagesStats.messagesByUser[m.author.id] = 1;
  bot.channel.messagesStats.averageLength = calculateAverageMessages(bot.channel.messagesStats.averageLength, bot.channel.messagesStats.messages, m.content.length);



  //IMAGES
  if ((m.embeds.length > 0 && m.embeds[0].type === "image") || (m.attachments.length > 0 && m.attachments[0].height > 16)) {
    //GUILD
    bot.guild.messagesStats.images++;
    if (bot.guild.messagesStats.imagesByUser[m.author.id] !== undefined)
      bot.guild.messagesStats.imagesByUser[m.author.id]++;
    else bot.guild.messagesStats.imagesByUser[m.author.id] = 1;
    if (bot.guild.messagesStats.imagesByChannel[m.channel.id] !== undefined)
      bot.guild.messagesStats.imagesByChannel[m.channel.id]++;
    else bot.guild.messagesStats.imagesByChannel[m.channel.id] = 1;

    bot.user.messagesStats.images++;
    if (bot.user.messagesStats.imagesByGuild[m.channel.guild.id] !== undefined)
      bot.user.messagesStats.imagesByGuild[m.channel.guild.id]++;
    else bot.user.messagesStats.imagesByGuild[m.channel.guild.id] = 1;
    if (bot.user.messagesStats.imagesByChannel[m.channel.id] !== undefined)
      bot.user.messagesStats.imagesByChannel[m.channel.id]++;
    else bot.user.messagesStats.imagesByChannel[m.channel.id] = 1;

    bot.channel.messagesStats.images++;
    if (bot.channel.messagesStats.imagesByUser[m.author.id] !== undefined)
      bot.channel.messagesStats.imagesByUser[m.author.id]++;
    else bot.channel.messagesStats.imagesByUser[m.author.id] = 1;
  }

  return {
    guild:   bot.guild,
    user:    bot.user,
    channel: bot.channel
  };
};

function calculateAverageMessages(avg, total, mLength) {
  return ((avg * total) + mLength) / (total + 1);
}
