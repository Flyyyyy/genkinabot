import * as vm2 from "vm2";

export const meta = {
    name:        "code",
    description: "Use code created by server admin !",
    author:      "Fly"
};

export const messageCreate = main;
export const messageUpdate = main;

async function main(bot, message) {
    if (!bot.guild.code.babelCode) //if there is no "babelified" code available
        return "OK";

    if (bot.guild.code.babelCode.includes("while") || bot.guild.code.babelCode.includes("for"))
        return "OK";

    if (bot.guild.code.isBugged)
        return "OK";

    let actionTokens = bot.guild.code.actions;

    const vm = new vm2.NodeVM({
        console: "off",
        wrapper: "none",
        timeout: 100,
        require: {
            external: false,
            builtin:  false
        },
        sandbox: {
            message: {
                author: {
                    avatar:           message.author.avatar,
                    avatarURL:        message.author.avatarURL,
                    bot:              message.author.bot,
                    createdAt:        message.author.createdAt,
                    defaultAvatar:    message.author.defaultAvatar,
                    defaultAvatarURL: message.author.defaultAvatarURL,
                    discriminator:    message.author.discriminator,
                    id:               message.author.id,
                    mention:          message.author.mention,
                    staticAvatarURL:  message.author.staticAvatarURL,
                    username:         message.author.username,

                    ban:   async (days) => { if (actionTokens > 0) { actionTokens--; return message.author.ban(days); } },
                    kick:  async () => { if (actionTokens > 0) { actionTokens--; return message.author.kick(); } },
                    unban: async () => { if (actionTokens > 0) { actionTokens--; return message.author.unban(); } },

                    addRole:    async (roleId) => { if (actionTokens > 0) { actionTokens--; return message.author.addRole(roleId); } },
                    removeRole: async (roleId) => { if (actionTokens > 0) { actionTokens--; return message.author.removeRole(roleId); } },

                    setMute:    async (isMute) => { if (actionTokens > 0) { actionTokens--; return message.author.edit({ mute: isMute }) } },
                    setDeaf:    async (isDeaf) => { if (actionTokens > 0) { actionTokens--; return message.author.edit({ deaf: isDeaf }) } },
                    setChannel: async (channelId) => { if (actionTokens > 0) { actionTokens--; return message.author.edit({ channelID: channelId }) } },
                    setNick:    async (nickname) => { if (actionTokens > 0) { actionTokens--; return message.author.edit({ nick: nickname }); } }
                },
                channel: {
                    createdAt: message.channel.createdAt,
                    mention:   message.channel.mention,
                    topic:     message.channel.topic,
                    id:        message.channel.id
                },
                content: message.content,
                guild:   {
                    id:          message.channel.guild.id,
                    joinedAt:    message.channel.guild.joinedAt,
                    large:       message.channel.guild.large,
                    memberCount: message.channel.guild.memberCount,
                    mfaLevel:    message.channel.guild.mfaLevel,
                    name:        message.channel.guild.name,
                    ownerID:     message.channel.guild.ownerID,
                    region:      message.channel.guild.region
                },

                addReaction:     async (reaction) => { if (actionTokens > 0) { actionTokens--; return message.addReaction(reaction); } },
                removeReactions: async () => { if (actionTokens > 0) { actionTokens--; return message.removeReactions(); } },

                delete: async () => { if (actionTokens > 0) { actionTokens--; return message.delete(); } },
                pin:    async () => { if (actionTokens > 0) { actionTokens--; return message.pin(); } },
                unpin:  async () => { if (actionTokens > 0) { actionTokens--; return message.unpin(); } }
            },
            bot: {
                sendMessage: async (text) => { if (actionTokens > 0) { actionTokens--; return message.channel.createMessage(text); } }
            }
        }
    });

    try {
        vm.run(bot.guild.code.source);
        bot.guild.code.errorMessage = null;
        return { guild: bot.guild }
    }
    catch (error) {
        bot.guild.code.isBugged = true;
        bot.guild.code.errorMessage = error.message;
        return { guild: bot.guild }
    }
}