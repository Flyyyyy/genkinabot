import { default as Discord } from "./discord";
import { default as Datastore } from "./datastore";

export { Discord, Datastore }