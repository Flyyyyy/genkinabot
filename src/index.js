"use strict";
// Libs && Constructors
import "babel-polyfill";
import { default as Eris } from "eris";
import * as Messages from "./util/messages/";
import * as Database from "./util/database/";
import * as Config from "./configs/";
import * as Modules from "./modules/";

// Main object
const Bot = {
  config:  Config, //require("./config/");
  modules: Modules, //require("./modules/");

  /**
   * @param {(ErisUser|ErisMessage)} user User to send the message
   * @param {string} message Message to send
   * @returns {ErisMessage} the sended message
   */
  private: async (user, message) => { //a function to easily send a private message to someone
    try {
      if (user.author) //if the user is a Eris.Message
        user = user.author; //set the user to message.author

      let dmChan = await user.getDMChannel() //get the DM channel ID

      let msg = await dmChan.createMessage(message); //send the message
      return msg;
    }
    catch (error) { //send message, within the DM channel
      Database.log({ //if the message send fail, log an error
        type:    "ERROR",
        cause:   "Trying to send MP",
        error:   { message: error.toString() || undefined },
        discord: { user: user, message: message }
      });
    }
  },

  log: Database.log,

  client: new Eris(Config.Discord.token, Config.Discord.erisConfig) //Eris client
};

Bot.client.onAny(async (event, arg1, arg2, arg3, arg4) => { //get any events from the Emitter
  let guildEris = null;

  if (event.startsWith("channel"))  //assume arg1 is a ErisChannel
    guildEris = arg1.guild || null; //set the guild to channel.guild 


  if (event.startsWith("guild")) //assume arg1 is a ErisGuild
    guildEris = arg1 || null;  //set the guild to guild

  if (event.startsWith("message")) //assume arg1 is a ErisMessage
    guildEris = arg1.channel.guild || null; //set the guild to message.guild

  if (event.startsWith("voiceChannel")) //assume arg1 is a ErisMember
    guildEris = arg1.guild || null; //set the guild to member.guild

  if (event === "messageCreate") { //If the event is a new message, and that message is sent by a bot, just ignore it
    if (arg1.author.bot)
      return;
    if (typeof arg1.channel.guild === "undefined")
      return;
  }

  if (guildEris === null) //If the event don't contain a guild object
    Object.keys(Bot.modules).forEach(async module => { //for each loaded module
      if (Bot.modules[module][event] !== undefined) { //if the module contains a event
        try {
          await Bot.modules[module][event](Bot, arg1, arg2, arg3, arg4); //try to use Module.event()
        }
        catch (error) { //if Module.event() throw an error
          Database.log({ //log it to database
            type:  "ERROR",
            cause: `Trying to use ${event} from ${Bot.modules[module].meta.name}`,
            error: error.toString()
          });
        }
      }
    });

  else {
    if (event.startsWith("message")) {
      let dbItems = await Database.getItems(arg1);
      Bot.user = dbItems.user;
      Bot.channel = dbItems.channel;
      Bot.guild = dbItems.guild;

      Bot.messages = new Messages.Messages(Bot);
    }

    let modulesCount = Object.keys(Bot.modules).length; //how many modules ?
    let moduleProcessed = 0; //counter for processed modules

    Object.keys(Bot.modules).forEach(async key => { //for each loaded module
      if (!Bot.guild.modules.enabled.includes(Bot.modules[key].meta.name)) { //if the guild dont use this module, skip
        modulesCount--;
        return;
      }

      if (Bot.modules[key][event] !== undefined)  //if the guild use the module
        try {
          let result = await Bot.modules[key][event](Bot, arg1, arg2, arg3, arg4) //try to use Module.event()
          moduleProcessed++;

          if (typeof result === "object") { //get the return of the module, update Bot object if necessary
            Bot.guild = result.guild || Bot.guild;
            Bot.user = result.user || Bot.user;
            Bot.channel = result.channel || Bot.channel;
          }
          else if (result !== "OK") //if the module didnt returned an object, or "OK" string, this isnt normal
            Database.log({ //log it to database
              type:  "ERROR",
              cause: `Trying to use ${event} from ${Bot.modules[key].meta.name}`,
              error: { message: "No return !" }
            });
        }
        catch (error) { //if Module.event() throw an error
          Database.log({ //log it to database
            type:  "ERROR",
            cause: `Trying to use ${event} from ${Bot.modules[key].meta.name}`,
            error: { message: error.toString(), stack: error.stack }
          });
        }
      if (moduleProcessed === modulesCount) {
        Database.save(Bot.user);
        Database.save(Bot.channel);
        Database.save(Bot.guild);
      }
    });
  }
});

Bot.client.connect().catch(error => Database.log({ //Connect to Discord websockets, if it fails log on DB
  type:  "ERROR",
  cause: "Error while connecting Eris",
  error: error.toString()
}));


process.on('uncaughtException', error => Database.log({//on uncaughtException, log it on db
  type:  "ERROR",
  cause: "Uncaught exception",
  error: { message: error.toString(), stack: error.stack }
}));

process.on("unhandledRejection", (error, promise) => {
  Database.log({//on unhandledRejection, log it on db
    type:  "ERROR",
    cause: "Unhandled promise rejection",
    error: { message: error.toString(), stack: error.stack }
  });

  if (error.stack.includes("Could not load the default credentials"))
    process.exit(1);
}); 