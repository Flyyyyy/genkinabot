FROM node:carbon-slim
WORKDIR /opt/genkinabot

#Copy files
ADD . /opt/genkinabot

RUN apt-get update -qqy && apt-get install -qqy \
        curl \
        python-dev \
        python-setuptools \
        apt-transport-https \
        lsb-release \
        openssh-client \
        git \
        build-essential \
    #Set environment variables
    && export CLOUD_SDK_VERSION=178.0.0 && cd / \
    #Install GCloud
    && easy_install -U pip \
    && pip install -U crcmod \
    && export CLOUD_SDK_REPO="cloud-sdk-$(lsb_release -c -s)" \
    && echo "deb https://packages.cloud.google.com/apt $CLOUD_SDK_REPO main" > /etc/apt/sources.list.d/google-cloud-sdk.list \
    && curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add - \
    && apt-get update && apt-get install -y google-cloud-sdk=${CLOUD_SDK_VERSION}-0 $INSTALL_COMPONENTS \
    && gcloud config set core/disable_usage_reporting true \
    && gcloud config set component_manager/disable_update_check true \
    && gcloud config set metrics/environment github_docker_image \
    #Yarn install modules
    && cd /opt/genkinabot \
    && yarn install \
    #Clean docker image
    && apt-get remove -y git build-essential lsb-release python-dev python-setuptools \
    && apt-get clean -y autoclean \
    && apt-get autoremove -y \
    && rm -rf /var/lib/apt /var/lib/dpkg /var/lib/cache /var/lib/log \
    #Check everything's fine
    && gcloud --version \
    && yarn --version \
    && node --version

CMD [ "yarn", "start" ]